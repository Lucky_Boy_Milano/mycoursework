<?php

namespace core;

class DBQuery
{
    protected $type;
    protected $fields;
    protected $where;
    protected $tableName;
    protected $insertingRow;
    protected $updatingRow;
    protected $isOneRow;
    protected $order;
    protected $limitNumber;
    protected $offsetNumber;
    public function __construct($tableName)
    {
        $this->tableName = $tableName;
        $this->type = null;
        $this->fields = '*';
        $this->where = [];
        $this->isOneRow = false;
        $this->order = [];
        $this->limitNumber = null;
        $this->offsetNumber = null;
        $this->like = null;
    }
    public function one()
    {
        $this->isOneRow = true;
        return $this;
    }
    public function isOne()
    {
        return $this->isOneRow;
    }
    public function insert($row)
    {
        $this->type = 'INSERT';
        $this->insertingRow = $row;
        return $this;
    }

    public function select($fields = '*')
    {
        $this->type = 'SELECT';
        $this->fields = $fields;
        return $this;
    }
    public function delete()
    {
        $this->type = 'DELETE';
        return $this;
    }
    public function update($row)
    {
        $this->type = 'UPDATE';
        $this->updatingRow = $row;
        return $this;
    }

    protected function generateWherePart($where)
    {
        $fieldsList = array_keys($where);
        $valuesList = array_values($where);
        $whereComponents = [];
        foreach ($fieldsList as $item) {
            array_push($whereComponents, "{$item} = :{$item}");
        }
        $wherePart = implode(' AND ', $whereComponents);
        return $wherePart;
    }
    public function where($condition)
    {
        if (is_string($condition))
            array_push($this->where, $condition);
        if (is_array($condition))
            $this->where = array_merge($this->where, $condition);
        return $this;
    }

    public function like($row)
    {
        foreach ($row as $key => $item) {
            $this->like = ' WHERE ' . $key . " LIKE \"%" . $item . "%\"";
        }
        return $this;
    }

    public function orderBy($condition)
    {
        if (is_string($condition))
            array_push($this->order, $condition);
        if (is_array($condition))
            $this->order = array_merge($this->order, $condition);
        return $this;
    }

    public function limit($count = 10)
    {
        if (is_numeric($count))
            $this->limitNumber = $count;
        return $this;
    }

    public function offset($count = 1)
    {
        if (is_numeric($count))
            $this->offsetNumber = $count;
        return $this;
    }

    protected function generateParamsArray($row)
    {
        $params = [];
        foreach ($row as $key => $item) {
            $params[':' . $key] = $item;
        }
        return $params;
    }

    public function getSQL()
    {
        switch ($this->type) {
            case 'UPDATE':
                $wherePart = $this->generateWherePart($this->where);
                $setPartArray = [];
                $params = [];
                foreach ($this->updatingRow as $key => $value) {
                    array_push($setPartArray, $key . ' = :' . $key);
                    $params[':' . $key] = $value;
                }
                foreach ($this->where as $key => $value) {
                    $params[':' . $key] = $value;
                }
                $setPartString = implode(', ', $setPartArray);
                $sql = "UPDATE {$this->tableName} SET {$setPartString} WHERE {$wherePart}";
                return ['sql' => $sql, 'params' => $params];
                break;

            case 'DELETE':
                $wherePart = $this->generateWherePart($this->where);
                $sql = "DELETE FROM {$this->tableName} WHERE {$wherePart}";
                $params = $this->generateParamsArray($this->where);
                return ['sql' => $sql, 'params' => $params];
                break;

            case 'SELECT':
                if (is_string($this->fields))
                    $fieldPart = $this->fields;
                else
                    if (is_array($this->fields))
                    $fieldPart = implode(',', $this->fields);
                else
                    return null;
                $sql = "SELECT {$fieldPart} FROM {$this->tableName}";
                if (!empty($this->where)) {
                    $wherePart = $this->generateWherePart($this->where);
                    $sql = $sql . " WHERE {$wherePart}";
                }
                if (!empty($this->order)) {
                    $itemsForOrder = implode(',', $this->order);
                    $sql = $sql . " ORDER BY {$itemsForOrder}";
                }
                if (!empty($this->limitNumber)) {
                    $sql = $sql . " LIMIT {$this->limitNumber}";
                }
                if (!empty($this->offsetNumber)) {
                    $sql = $sql . " OFFSET {$this->offsetNumber}";
                }
                if (!empty($this->like)) {
                    $sql = $sql . $this->like;
                }
                $params = $this->generateParamsArray($this->where);
                return ['sql' => $sql, 'params' => $params];
                break;

            case 'INSERT':
                $fieldaList = array_keys($this->insertingRow);
                $valuesList = array_values($this->insertingRow);
                $fieldaListString = implode(', ', $fieldaList);
                $valuesParamsList = [];
                $params = [];
                foreach ($this->insertingRow as $key => $item) {
                    array_push($valuesParamsList, ':' . $key);
                    $params[':' . $key] = $item;
                }
                $valuesListString = implode(', ', $valuesParamsList);
                $sql = "INSERT INTO {$this->tableName} ({$fieldaListString}) VALUES ({$valuesListString})";
                return ['sql' => $sql, 'params' => $params];
                break;
                return null;
        }
    }
}
