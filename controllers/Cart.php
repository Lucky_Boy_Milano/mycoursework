<?php

namespace controllers;

use models\Users;

/**
 * Контроллер для модуля News
 * @param controllers
 */
class Cart extends \core\Controller
{
    protected $ModelCart;
    protected $ModelProduct;
    protected $ModelUsers;
    protected $ModelOrder;

    public function __construct()
    {
        $this->ModelCart = new \models\Cart();
        $this->ModelProduct = new \models\Product();
        $this->ModelUsers = new \models\Users();
        $this->ModelOrder = new \models\Order();
    }

    public function actionIndex()
    {
        $productsInCart = $this->ModelCart->getProducts();
        $products = [];
        if ($productsInCart) {
            $productsIds = array_keys($productsInCart);
            $products = $this->ModelProduct->getProductsByIds($productsIds);
        }

        $totalPrice = $this->ModelCart->getTotalPrice($products);
        return $this->render(
            'index',
            [
                'products' => $products,
                'totalPrice' => $totalPrice,
                'productsInCart' => $productsInCart
            ],
            ['Title' => 'Корзина']
        );
    }

    public function actionChange($id, $count)
    {
        $this->ModelCart->changeCount($id, $count);
        header("Location: /cart");
    }
    public function actionAdd($id)
    {
        $this->ModelCart->addProduct($id);

        $ref = $_SERVER['HTTP_REFERER'];
        header("Location: $ref");
    }
    public function actionAddajax($id)
    {
        $this->ModelCart->addProduct($id);
        return $this->ModelCart->countItems();
    }
    public function actionCheckout()
    {
        $errors = [];

        $result = false;

        $productsInCart = $this->ModelCart->getProducts();

        $productsIds = array_keys($productsInCart);
        $products = $this->ModelProduct->getProductsByIds($productsIds);
        $totalPrice = $this->ModelCart->getTotalPrice($products);
        $totalQuantity = $this->ModelCart->countItems();

        if ($productsInCart == false) {
            return $this->render(
                'index',
                [
                    'products' => $products,
                    'totalPrice' => $totalPrice,
                    'productsInCart' => $productsInCart,
                    'message' => 'Спочатку додайте товар до корзини.'
                ],
                ['Title' => 'Корзина']
            );
        } else {

            if ($this->isPost()) {

                $id = $this->ModelUsers->checkLogged();
                if (!is_numeric($id)) {
                    $id = 0;
                }

                $post = $this->postFilter(['login', 'firstname', 'lastname', 'address', 'comment']);

                if (!($this->ModelUsers->checkNameLength($post['firstname'])))
                    $errors[] = "Ім'я повинно містити більше двох символів";

                if (!($this->ModelUsers->checkNameLength($post['lastname'])))
                    $errors[] = "Прізвище повинно містити більше двох символів";

                if (!($this->ModelUsers->checkPasswordLength($post['address'])))
                    $errors[] = "Адреса повинна містити більше 6 символів";

                if (!($this->ModelUsers->checkEmail($post['login'])))
                    $errors[] = "Електронна пошта вказана не вірно";

                if (empty($errors)) {
                    $this->ModelOrder->createOrder(
                        $id,
                        $post['firstname'],
                        $post['lastname'],
                        $post['address'],
                        $post['comment'],
                        $productsInCart,
                        $totalPrice

                    );
                    $this->ModelCart->clearCart();

                    return $this->render(
                        'index',
                        [
                            'products' => $products = [],
                            'totalPrice' => $totalPrice = 0,
                            'productsInCart' => $productsInCart = 0,
                            'message' => 'Дякуємо, ваше замовлення буде оброблено в найкоротший термін.'
                        ],
                        ['Title' => 'Корзина']
                    );
                } else {
                    return $this->render(
                        'checkout',
                        [
                            'firstname' => $post['firstname'],
                            'lastname' => $post['lastname'],
                            'login' => $post['login'],
                            'address' => $post['address'],
                            'comment' => $post['comment'],
                            'totalPrice' =>  $totalPrice,
                            'totalQuantity' => $totalQuantity,
                            'errors' => $errors
                        ],
                        ['Title' => "Оформлення"]
                    );
                }
            } else {
                $firstname = false;
                $lastname = false;
                $login = false;
                $address = false;
                $comment = false;

                if (Users::isGuest()) {
                } else {
                    $userId = $this->ModelUsers->checkLogged();
                    $user = $this->ModelUsers->getUserById($userId);

                    $firstname = $user['firstname'];
                    $lastname = $user['lastname'];
                    $login = $user['login'];
                }
            }
            return $this->render('checkout', [
                'errors' => $errors,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'login' => $login,
                'totalPrice' =>  $totalPrice,
                'totalQuantity' => $totalQuantity,
                'address' => null,
                'comment' => null
            ], ['Title' => "Оформлення"]);
        }
    }

}
