<?php

namespace controllers;

/**
 * Контроллер для модуля Profile
 * @param controllers
 */
class Profile extends \core\Controller
{
    protected $Model;
    protected $ModelOrder;
    protected $ModelProduct;

    public function __construct()
    {
        $this->Model = new \models\Users();
        $this->ModelOrder = new \models\Order();
        $this->ModelProduct = new \models\Product();
    }
    /**
     * Відображення початкової сторінки модуля
     */
    public function actionIndex()
    {
        $profile = $this->Model->checkLogged();

        $user = $this->Model->getUserById($profile);
        $orders = $this->ModelOrder->getOrdersById($user['id']);
        return $this->render('index', ['orders' => $orders, 'firstname' => $user['firstname'], 'lastname' => $user['lastname'], 'login' => $user['login'], 'role' => $user['role']], ['Title' => 'Профіль']);
    }

    public function actionEdit()
    {
        $profile = $this->Model->checkLogged();
        $user = $this->Model->getUserById($profile);
        $orders = $this->ModelOrder->getOrdersById($user['id']);
        $post = $this->postFilter(['login', 'password', 'firstname', 'lastname', 'confirmPassword']);
        $errors = [];

        if ($this->isPost()) {
            if (!($this->Model->checkNameLength($post['firstname'])))
                $errors[] = "Ім'я повинно містити більше двох символів";

            if (!($this->Model->checkNameLength($post['lastname'])))
                $errors[] = "Прізвище повинно містити більше двох символів";

            if (!($this->Model->checkPasswordLength($post['password'])))
                $errors[] = "Пароль повинно містити більше 6 символів";

            if (!($this->Model->checkEmail($post['login'])))
                $errors[] = "Електронна пошта вказана не вірно";

            if ($user['login'] !=  $post['login']) {
                if (($this->Model->checkEmailExists($post['login'])))
                    $errors[] = "Така електронна пошта уже використовується іншим користувачем";
            }

            if (empty($errors)) {
                $this->Model->Edit($post['login'], $post['password'], $post['firstname'], $post['lastname']);
                return $this->render('index', ['orders' => $orders, 'edited' => true, 'firstname' => $user['firstname'], 'lastname' => $user['lastname'], 'login' => $user['login'], 'role' => $user['role']], ['Title' => 'Профіль']);
            }
        }
        return $this->render('edit', ['login' => $user['login'], 'firstname' => $user['firstname'], 'lastname' => $user['lastname'], 'password' => $user['password'], 'errors' => $errors], ['Title' => 'Редагування']);
    }
    public function actionView($id)
    {
        $order = $this->ModelOrder->getOrderById($id);

        $orderedProducts = (array) json_decode($order['products']);
        $productsIds = array_keys($orderedProducts);
        $products = $this->ModelProduct->getProductsByIds($productsIds);
        return $this->render('view', ['order' => $order, 'products' => $products, 'countProducts' => $orderedProducts], ['Title' => 'Замовлення']);
    }
}
