<?php

namespace controllers;


class Error extends \core\Controller
{
    /**
     * Відображення початкової сторінки модуля
     */
    public function action404()
    {
        return $this->render('404', [], ['Title' => '404']);
    }
}
