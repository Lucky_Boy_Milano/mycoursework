<?php

namespace controllers;

/**
 * Контроллер для модуля News
 * @param controllers
 */
class News extends \core\Controller
{
    protected $ModelNews;
    protected $ModelPagination;

    public function __construct()
    {
        $this->ModelNews = new \models\News();
    }
    /**
     * Відображення початкової сторінки модуля
     */
    public function actionIndex($page = 1)
    {
        $News = $this->ModelNews->getNews(6,$page);
        $count = $this->ModelNews->getCountNews();
        $this->ModelPagination = new \models\Pagination($count, $page, 4, 'page=');

        return $this->render('index', ['news' => $News, 'pagination' => $this->ModelPagination], ['Title' => 'Новини']);
    }
}
