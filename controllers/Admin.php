<?php

namespace controllers;

/**
 * Контроллер для модуля Admin
 * @param controllers
 */
class Admin extends \core\Controller
{
    protected $ModelAdmin;
    protected $ModelProduct;
    protected $ModelUsers;
    protected $ModelOrder;
    protected $ModelCategory;
    protected $ModelSubscribe;
    protected $ModelNews;

    public function __construct()
    {
        $this->ModelCategory = new \models\Category();
        $this->ModelAdmin = new \models\Admin();
        $this->ModelProduct = new \models\Product();
        $this->ModelUsers = new \models\Users();
        $this->ModelOrder = new \models\Order();
        $this->ModelSubscribe = new \models\Subscribe();
        $this->ModelNews = new \models\News();
    }

    public function actionIndex()
    {
        $this->ModelAdmin->checkAdmin();
        return $this->render('index', ['count' => 20], ['Title' => 'Панель керування']);
    }

    public function actionProducts()
    {
        $this->ModelAdmin->checkAdmin();

        $productsList = $this->ModelProduct->getProductsList();

        return $this->render('products', ['products' => $productsList], ['Title' => 'Панель керування']);
    }

    public function actionProductupdate($id)
    {
        $this->ModelAdmin->checkAdmin();
        $categoriesList = $this->ModelCategory->getCategoriesList();

        $product = $this->ModelProduct->getProductById($id);

        if ($this->isPost()) {
            $post = $this->postFilter([
                'name',
                'category_id',
                'code',
                'price',
                'image',
                'description',
                'is_recomended',
                'status',
            ]);
            $this->ModelProduct->updateProduct($id, $post);

            if (is_uploaded_file($_FILES['image']['tmp_name'])) {
                move_uploaded_file($_FILES['image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/products/{$id}.jpg");
            }

            header("Location: /admin/products");
        }

        return $this->render(
            'productupdate',
            [
                'categories' => $categoriesList,
                'product' => $product
            ],
            ['Title' => 'Редагування товару']
        );
    }
    public function actionUsers()
    {
        $this->ModelAdmin->checkAdmin();

        $users = $this->ModelUsers->getAllUsers();

        return $this->render('users', ['users' => $users], ['Title' => 'Користувачі']);
    }

    public function actionUserdelete($id)
    {
        $this->ModelAdmin->checkAdmin();

        $this->ModelUsers->deleteUserById($id);

        header("Location: /admin/users");
    }

    public function actionUserupdate($id, $role)
    {
        $this->ModelAdmin->checkAdmin();

        $this->ModelUsers->updateUserRole($id, $role);

        header("Location: /admin/users");
    }

    public function actionNews()
    {
        $this->ModelAdmin->checkAdmin();

        $News = $this->ModelNews->getAllNews();

        return $this->render('news', ['news' => $News], ['Title' => 'Новини']);
    }
    public function actionProductadd()
    {
        $this->ModelAdmin->checkAdmin();

        $categoriesList = $this->ModelCategory->getCategoriesList();

        if ($this->isPost()) {
            $post = $this->postFilter([
                'name',
                'category_id',
                'code',
                'price',
                'description',
                'is_recomended',
                'status'
            ]);

            $id = $this->ModelProduct->createProduct($post);

            if ($id) {
                if (is_uploaded_file($_FILES['image']['tmp_name'])) {
                    move_uploaded_file($_FILES['image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/products/{$id['id']}.jpg");
                }
            }

            header("Location: /admin/products");
        }

        return $this->render('productadd', ['categories' => $categoriesList], ['Title' => 'Додавання товару']);
    }

    public function actionProductdelete($id)
    {
        $this->ModelAdmin->checkAdmin();

        $this->ModelProduct->deleteProductById($id);

        header("Location: /admin/products");
    }
    //закази
    public function actionOrders()
    {
        $this->ModelAdmin->checkAdmin();

        $orders = $this->ModelOrder->getAllOrders();

        return $this->render('orders', ['orders' => $orders], ['Title' => 'Замовлення']);
    }

    public function actionOrderupdate($status, $id)
    {
        $this->ModelAdmin->checkAdmin();

        $this->ModelOrder->changeStatus($id, $status);

        header("Location: /admin/orders");
    }
    public function actionOrderdelete($id)
    {
        $this->ModelAdmin->checkAdmin();

        $this->ModelOrder->deleteOrderById($id);

        header("Location: /admin/orders");
    }

    public function actionOrderview($id)
    {
        $this->ModelAdmin->checkAdmin();

        $order = $this->ModelOrder->getOrderById($id);

        $orderedProducts = (array) json_decode($order['products']);
        $productsIds = array_keys($orderedProducts);
        $products = $this->ModelProduct->getProductsByIds($productsIds);
        return $this->render(
            'orderview',
            [
                'order' => $order,
                'products' => $products,
                'countProducts' => $orderedProducts
            ],
            ['Title' => 'Замовлення']
        );
    }
    //підписники
    public function actionSubscribe()
    {
        $this->ModelAdmin->checkAdmin();
        $folowers = $this->ModelSubscribe->getAllSubscribe();
        return $this->render(
            'subscribe',
            [
                'folowers' => $folowers,
            ],
            ['Title' => 'Підписники']
        );
    }
    public function actionSubscribedelete($id)
    {
        $this->ModelAdmin->checkAdmin();

        $this->ModelSubscribe->deleteSubscribe($id);
        header("Location: /admin/subscribe");
    }
    //Катигорії
    public function actionCategory()
    {
        $this->ModelAdmin->checkAdmin();

        $categoriesList = $this->ModelCategory->getCategoriesList();

        return $this->render('category', ['categories' => $categoriesList], ['Title' => 'Категорії']);
    }

    public function actionCategorydelete($id)
    {
        $this->ModelAdmin->checkAdmin();

        $this->ModelCategory->deleteCategoryById($id);

        header("Location: /admin/category");
    }

    public function actionCategoryadd($id)
    {
        $this->ModelAdmin->checkAdmin();

        if ($this->isPost()) {
            $post = $this->postFilter([
                'name'
            ]);
            $this->ModelCategory->addCategory($post);
        }

        header("Location: /admin/category");
    }


    public function actionNewadd()
    {
        $this->ModelAdmin->checkAdmin();

        if ($this->isPost()) {
            $post = $this->postFilter([
                'title',
                'description',
            ]);

            $id = $this->ModelNews->createNews($post);
            if ($id) {
                if (is_uploaded_file($_FILES['image']['tmp_name'])) {
                    move_uploaded_file($_FILES['image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/news/{$id['id']}.jpg");
                }
            }

            header("Location: /admin/news");
        }

        return $this->render('newadd', [], ['Title' => 'Додавання новини']);
    }
    public function actionNewupdate($id)
    {
        $this->ModelAdmin->checkAdmin();

        $news = $this->ModelNews->getNewsById($id);

        if ($this->isPost()) {
            $post = $this->postFilter([
                'title',
                'description',
            ]);
            $this->ModelNews->updateNew($id, $post);
                if (is_uploaded_file($_FILES['image']['tmp_name'])) {
                    move_uploaded_file($_FILES['image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/upload/images/news/{$id}.jpg");
                }

            header("Location: /admin/news");
        }
        return $this->render('newupdate', ['information' => $news,'id' => $id], ['Title' => 'Редагування']);
    }
    public function actionNewdelete($id)
    {
        $this->ModelAdmin->checkAdmin();

        $this->ModelNews->deleteNewById($id);

        header("Location: /admin/news");
    }
}
