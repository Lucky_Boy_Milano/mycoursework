<?php

namespace controllers;

class Site extends \core\Controller
{
    protected $ModelSubscribe;
    protected $ModelCategory;
    protected $ModelProduct;
    protected $ModelNews;

    public function __construct()
    {
        $this->ModelSubscribe = new \models\Subscribe();
        $this->ModelCategory = new \models\Category();
        $this->ModelProduct = new \models\Product();
        $this->ModelNews = new \models\News();
    }

    public function actionIndex()
    {

        $news = $this->ModelNews->getNews(3,1);
        $categories = $this->ModelCategory->getCategoriesList();
        $products = $this->ModelProduct->getRecomendedProducts();
        return $this->render(
            'index',
            [
                'categories' => $categories,
                'products' => $products,
                'news' => $news
            ],
            ['Title' => 'Головна сторінка']
        );
    }

    public function actionSubscribe()
    {
        if ($this->isPost()) {
            $post = $this->postFilter(['email']);
            $this->ModelSubscribe->addSubscribe($post['email']);
        }
        $ref = $_SERVER['HTTP_REFERER'];
        header("Location: $ref");
    }
}
