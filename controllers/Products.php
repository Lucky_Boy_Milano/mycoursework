<?php

namespace controllers;

class Products extends \core\Controller
{
    protected $ModelCategory;
    protected $ModelProduct;
    protected $ModelPagination;

    public function __construct()
    {
        $this->ModelCategory = new \models\Category();
        $this->ModelProduct = new \models\Product();
    }

    public function actionIndex()
    {
        header("Location: /products/category");
    }

    public function actionCategory($id, $page)
    {
        if (!is_numeric($page)) $page = 1;
        $categories = $this->ModelCategory->getCategoriesList();
        $title =  $categories[$id]['name'];
        if ($id == null) {
            $categoryProducts = $this->ModelProduct->getAllProducts($page);
            $title = 'Товари';
        } else {
            $categoryProducts = $this->ModelProduct->getProductsByCategory($id, $page);
        }
        $count = intval($this->ModelProduct->getCountProductsByCategory($id)['count']);
        $this->ModelPagination = new \models\Pagination($count, $page, 9, 'page=');
        return $this->render(
            'index',
            [
                'categories' => $categories,
                'products' => $categoryProducts,
                'id' => $id,
                'page' => $page,
                'pagination' => $this->ModelPagination
            ],
            ['Title' => $title]
        );
    }

    public function actionProduct($id)
    {
        if ($id === null) $id = 0;
        $categories = $this->ModelCategory->getCategoriesList();
        $product = $this->ModelProduct->getProductInfo($id);
        $category = $categories[$product['category_id']]['name'];


        return $this->render(
            'product',
            ['category' => $category, 'product' => $product],
            ['Title' => 'Продукт']
        );
    }

    public function actionSearch()
    {
        $categories = $this->ModelCategory->getCategoriesList();
        if ($this->isPost()) {
            $post = $this->postFilter(['search']);
            $products = $this->ModelProduct->getProductsByName($post['search']);
            return $this->render(
                'index',
                [
                    'categories' => $categories,
                    'products' => $products,
                    'id' => 0,
                    'page' => 0,
                    'pagination' => null,
                    'search' => $post['search']
                ],
                ['Title' => 'Результати пошуку']
            );
        }
        header("Location: \products");
    }
}
