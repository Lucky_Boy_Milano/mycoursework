<?php

namespace controllers;

use Error;

class Users extends \core\Controller
{
    protected $Model;

    public function __construct()
    {
        $this->Model = new \models\Users();
    }

    public function actionIndex()
    {
    }

    public function actionLogin()
    {
        $res = false;
        $errors = [];
        $existence = false;
        if ($this->isPost()) {
            $post = $this->postFilter(['login', 'password']);

            if (!($this->Model->checkEmail($post['login'], $post['confirmPassword'])))
                $errors[] = "Електронна адреса вказана не вірно";

            if (($this->Model->checkEmailExists($post['login'])))
                $existence = true;
            else
                $errors[] = "Користувача з такою електронною адресою не знайдено";



            if (empty($errors))
                $res = $this->Model->Authenticate($post['login'], $post['password']);

            if ($res == false && $existence == true)
                $errors[] = "Пароль вказано не вірно";
        }
        if ($res != false) {
            $this->Model->auth($res);

            header("Location: /profile/");
        } else {
            return $this->render('login', ['login' => $post['login'], 'errors' => $errors], ['Title' => 'Авторизація']);
        }
    }
    public function actionRegister()
    {
        $errors = [];
        if ($this->isPost()) {
            $post = $this->postFilter(['login', 'password', 'firstname', 'lastname', 'confirmPassword']);

            if (!($this->Model->checkNameLength($post['firstname'])))
                $errors[] = "Ім'я повинно містити більше двох символів";

            if (!($this->Model->checkNameLength($post['lastname'])))
                $errors[] = "Прізвище повинно містити більше двох символів";

            if (!($this->Model->checkPasswordLength($post['password'])))
                $errors[] = "Пароль повинно містити більше 6 символів";

            if (!($this->Model->checkConfirmPassword($post['password'], $post['confirmPassword'])))
                $errors[] = "Паролі не співпадають";

            if (!($this->Model->checkEmail($post['login'])))
                $errors[] = "Електронна пошта вказана не вірно";

            if (($this->Model->checkEmailExists($post['login'])))
                $errors[] = "Така електронна пошта уже використовується іншим користувачем";

            if (empty($errors)) {
                $this->Model->Register($post['login'], $post['password'], $post['firstname'], $post['lastname']);
                header("Location: /users/login");
            }
        }
        return $this->render('register', ['post' => $post, 'errors' => $errors], ['Title' => 'Створення аккаунту']);
    }
    public function actionLogout()
    {
        unset($_SESSION['user']);
        header("Location: /");
    }
}
