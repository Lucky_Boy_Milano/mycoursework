    <section class="breadcrumb-section set-bg" data-setbg="../../template/img/breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Корзина покупок</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="shoping-cart spad">
        <div class="container">
            <?php if (!empty($message)) : ?>
                <div class="alert alert-success alert-dismissible fade show col-12 mt-3" role="alert">
                    <?php print($message) ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th class="shoping__product">Products</th>
                                    <th>Ціна</th>
                                    <th>Кількість</th>
                                    <th>Всього</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($products as $product) : ?>
                                    <tr>
                                        <td class="shoping__cart__item">
                                            <img class="card-img col-lg-3 col-md-6 col-sm-12" src="<?php echo (\models\Product::getImage($product['id'])) ?>" alt="">
                                            <h5><?php print($product['name']) ?></h5>
                                        </td>
                                        <td class="shoping__cart__price">
                                            <?php print($product['price']) ?> грн
                                        </td>
                                        <td class="shoping__cart__quantity">
                                            <div class="quantity">
                                                <div class="">
                                                    <input onchange="document.location='/cart/change?id=<?php echo ($product['id']) ?>&count='+ document.getElementById(<?php echo ($product['id']) ?>).value;" type="number" name="qty" id="<?php echo ($product['id']) ?>" maxlength="12" value="<?php echo ($productsInCart[$product['id']]) ?>" title="Кількість:" class="form-control ml-5 col-6 countProduct">
                                                </div>
                                            </div>
                                        </td>
                                        <td class="shoping__cart__total">
                                            <?php echo ($productsInCart[$product['id']] * $product['price']) ?> грн
                                        </td>
                                        <td class="shoping__cart__item__close">
                                            <a onclick="document.location='/cart/change?id=<?php echo ($product['id']) ?>&count=0'"> <span class="icon_close"></span></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="shoping__cart__btns">
                        <a href="/products/category" class="primary-btn cart-btn">Продовжити покупки</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="shoping__checkout">
                        <h5>Загальна сума</h5>
                        <ul>
                            <li>Всього <span><?php print($totalPrice) ?> грн</span></li>
                        </ul>
                        <a href="/cart/checkout" class="primary-btn">Оформити замовлення</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
