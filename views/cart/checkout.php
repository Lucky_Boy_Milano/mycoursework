       <section class="breadcrumb-section set-bg" data-setbg="../../template/img/breadcrumb.jpg">
           <div class="container">
               <div class="row">
                   <div class="col-lg-12 text-center">
                       <div class="breadcrumb__text">
                           <h2>Checkout</h2>
                           <div class="breadcrumb__option">
                               <span>Оформлення замовлення</span>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </section>

       <section class="checkout spad">
           <div class="container">
               <div class="checkout__form">
                   <h4>Реквізити</h4>
                   <form method="POST" action="/cart/checkout">
                       <div class="row">
                           <div class="col-lg-8 col-md-6">
                               <div class="row">

                                   <div class="col-lg-6">
                                       <div class="checkout__input">
                                           <p>Ім'я</p>
                                           <input id="firstname" name="firstname" type="text" value="<?php echo ($firstname) ?>">
                                       </div>
                                   </div>
                                   <div class="col-lg-6">
                                       <div class="checkout__input">
                                           <p>Прізвище</p>
                                           <input id="lastname" name="lastname" type="text" value="<?php echo ($lastname) ?>">
                                       </div>
                                   </div>
                               </div>
                               <div class="checkout__input">
                                   <p>Адреса</p>
                                   <input id="address" name="address" type="text" placeholder="" class="checkout__input__add" value="<?php echo ($address) ?>">
                               </div>
                               <div class="row">
                                   <div class="col-lg-12">
                                       <div class="checkout__input">
                                           <p>Email</p>
                                           <input id="login" name="login" type="text" value="<?php echo ($login) ?>">
                                       </div>
                                   </div>
                               </div>
                               <div class="checkout__input">
                                   <p>Коментар</p>
                                   <input id="comment" name="comment" type="text" placeholder="" value="<?php echo ($comment) ?>">
                               </div>
                           </div>
                           <div class="col-lg-4 col-md-6">
                               <?php foreach ($errors as $error) : ?>
                                   <div class="alert alert-danger alert-dismissible fade show col-12" role="alert">
                                       <?php print($error); ?>
                                       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                           <span aria-hidden="true">&times;</span>
                                       </button>
                                   </div>
                               <?php endforeach; ?>
                               <div class="checkout__order">
                                   <h4>Замовлення</h4>
                                   <div class="checkout__order__products">Кількість товарів:<span><?php print($totalQuantity) ?></span></div>
                                   <div class="checkout__order__total">Сума: <span><?php print($totalPrice) ?> грн</span></div>
                                   <button type="submit" class="site-btn">Оформити</button>
                               </div>
                           </div>
                       </div>
                   </form>
               </div>
           </div>
       </section>