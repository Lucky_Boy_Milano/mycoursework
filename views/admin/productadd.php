<div class="container col-lg-6 text-center">
    <div class="col-12 d-flex justify-justify-content-between">
        <a href="/admin/products" class="btn shadow"><i class=" fa fa-arrow-left"></i></a>
    </div>
    <div class="">
        <h2 class="mb-4">Додавання товару</h2>
        <form сlass="login_form" action="/admin/productadd" method="post" enctype="multipart/form-data">
            <table class="table">
                <tr>
                    <th> Назва товару</th>
                    <th><input class="form-control" type="text" name="name" placeholder="" value=""></th>
                </tr>
                <tr>
                    <th>
                        Артикул

                    </th>
                    <th>
                        <input class="form-control" type="number" name="code" placeholder="" value="">
                    </th>
                </tr>
                <tr>
                    <th>Ціна</th>
                    <th> <input class="form-control" type="number" name="price" placeholder="" value=""></th>
                </tr>
                <tr>
                    <th>Категорія</th>
                    <th> <select class="" name="category_id" id="">
                            <?php if (is_array($categories)) : ?>
                                <?php foreach ($categories as $category) : ?>
                                    <option value="<?php echo ($category['id']); ?>">
                                        <?php echo $category['name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select></th>
                </tr>
                <tr>
                    <th>Додати в рекомендації</th>
                    <th> <select class="" name="is_recomended" id="">
                            <option value="1" selected>Так</option>
                            <option value="0">Ні</option>
                        </select></th>
                </tr>
                <tr>
                    <th>Відображення на сайті</th>
                    <th> <select class="" name="status" id="">
                            <option value="1" selected>Так</option>
                            <option value="0">Ні</option>
                        </select></th>
                </tr>
                <tr>
                    <th>Опис товару</th>
                    <th><textarea class="form-control col-11 ml-4" name="description" id="" cols="30" rows="10"></textarea></th>
                </tr>
                <tr>
                    <th>Зображення товару</th>
                    <th><input class="form-control m-3" type="file" name="image"></th>
                </tr>


            </table>
            <button class="btn btn-success shadow mb-3">Додати <i class="fa fa-check" aria-hidden="true"></i></button>
        </form>
    </div>
</div>
</div>