<div class="container">
    <a href="/admin" class="btn shadow"><i class=" fa fa-arrow-left"></i></a>
    <a class="btn m-3 shadow" href="/admin/productadd"><i class="fa fa-plus" aria-hidden="true"></i></a>
    <table class="table-borderless table">
        <tr>
            <th>ІD товару</th>
            <th>Назва товару</th>
            <th>Ціна товару</th>
            <th></th>
            <th></th>
        </tr>
        <?php foreach ($products as $product) : ?>
            <tr>
                <th><?php print($product['id']) ?></th>
                <th><?php print($product['name']) ?></th>
                <th><?php print($product['price']) ?> грн</th>
                <th><a href="/admin/productupdate?id=<?php echo ($product['id']) ?>" class="btn p-2 shadow"><i class="fa fa-pencil " aria-hidden="true"></i></a></th>
                <th><button onclick="if (confirm('Ви дійсно хочете видалити цей товар?'))document.location='/admin/productdelete?id=<?php echo ($product['id']) ?>'" class="shadow p-2 btn"><i class="fa fa-trash" aria-hidden="true"></i></button></th>
            </tr>
        <?php endforeach; ?>
    </table>
</div>