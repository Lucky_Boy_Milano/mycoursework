<div class="container col-lg-6 ">
    <a href="/admin" class="btn shadow mb-2"><i class=" fa fa-arrow-left"></i></a>
    <h3 class="text-center m-3">Категорії</h3>
    <form сlass="login_form" action="/admin/categoryadd" method="post" enctype="multipart/form-data">
        <table class="table">
            <tr>
                <td>Додати нову категорію</td>
                <td><input name="name" class=" form-control" type="text"></td>
                <td>
                    <button class="btn shadow" type="submit"><i class="fa fa-check" aria-hidden="true"></i></button>
                </td>
            </tr>
        </table>
    </form>

    <table class="table">

        <tr>
            <th>ІD категорії</th>
            <th>Назва категорії</th>
            <th></th>
        </tr>
        <?php foreach ($categories as $category) : ?>
            <tr>
                <th><?php print($category['id']) ?></th>
                <th><?php print($category['name']) ?></th>
                <th><button onclick="if (confirm('Ви дійсно хочете видалити цю категорію?'))document.location='/admin/categorydelete?id=<?php echo ($category['id']) ?>'" class="shadow p-2 btn"><i class="fa fa-trash" aria-hidden="true"></i></button></th>
            </tr>
        <?php endforeach; ?>
    </table>

</div>