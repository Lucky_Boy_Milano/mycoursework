<div class="container ">
    <a href="/admin" class="btn shadow m-3"><i class=" fa fa-arrow-left"></i></a>

    <table class="table">
        <tr>
            <th>ІD пошти</th>
            <th>Електрона пошта</th>
            <th></th>
        </tr>
        <?php foreach ($folowers as $folower) : ?>
            <tr>
                <th><?php print($folower['id']) ?></th>
                <th><?php print($folower['email']) ?></th>
                <th><button onclick="if (confirm('Ви дійсно хочете видалити цю електронну пошту?'))document.location='/admin/subscribedelete?id=<?php echo ($folower['id']) ?>'" class="btn shadow"><i class="fa fa-trash" aria-hidden="true"></i></button></th>
            </tr>
        <?php endforeach; ?>
    </table>
</div>