<div class="col-12 text-center m-1 d-flex justify-content-center">
    <div class="blog_right_sidebar col-lg-6 col-md-9 col-sm-12">
        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action active bg-success border-success">
                <h4 class="text-reset">Панель адміністратора</h4>
            </a>
            <a href="/admin/products" class="list-group-item list-group-item-action">
                <p>Керування товарами</p>
            </a>
            <a href="/admin/news" class="list-group-item list-group-item-action">
                <p>Редагування новин</p>
            </a>
            <a href="/admin/category" class="list-group-item list-group-item-action">
                <p>Керування категоріями</p>
            </a>
            <a href="/admin/orders" class="list-group-item list-group-item-action">
                <p>Керування замовленнями</p>
            </a>
            <a href="/admin/users" class="list-group-item list-group-item-action">
                <p>Cписок користувачів</p>
            </a>
            <a href="/admin/subscribe" class="list-group-item list-group-item-action">
                <p>Електронні пошти підписників</p>
            </a>
        </div>
    </div>
</div