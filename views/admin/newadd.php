<div class="container col-lg-6 text-center">
    <div class="col-12 d-flex justify-justify-content-between">
        <a href="/admin/news" class="btn shadow"><i class=" fa fa-arrow-left"></i></a>
    </div>
    <div class="login-form">
        <h2 class="mb-4">Додавання новини</h2>
        <form сlass="login_form" action="/admin/newadd" method="post" enctype="multipart/form-data">
            <table class="table">
                <tr>
                    <th>Заголовок</th>
                    <th><input class="form-control" type="text" name="title" placeholder="" value=""></th>
                </tr>
                <tr>
                    <th>Інформація</th>
                    <th>
                        <textarea class="form-control" name="description" id="" cols="30" rows="10"></textarea>

                    </th>
                </tr>
                <tr>
                    <th>Зображення</th>
                    <th>
                        <input class="form-control m-3" type="file" name="image">
                    </th>
                </tr>
            </table>
            <button class="btn btn-success mb-3">Додати <i class="fa fa-check" aria-hidden="true"></i></button>
        </form>
    </div>
</div>
</div>