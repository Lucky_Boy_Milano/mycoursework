<div class="container col-lg-6  login_form_inner register_form_inner">
    <div class="col-12 d-flex justify-justify-content-between">
        <a href="/admin/orders" class="btn shadow"><i class=" fa fa-arrow-left"></i></a>
    </div>
    <h2 class="text-center">Перегляд замовлення</h2>
    <table class="col-11 m-3 table">

        <tr>
            <td>Статус замовлення</td>
            <td>
                <select class="ml-5" onchange="document.location='/admin/orderupdate?id=<?php echo ($order['id']) ?>&status='+ document.getElementById(<?php echo ($order['id']) ?>).value;" name="" id="<?php echo ($order['id']) ?>">
                    <option <?php if ($order['status'] == '0') echo 'selected' ?> value="0">Нове замовлення</option>
                    <option <?php if ($order['status'] == '1') echo 'selected' ?> value="1">В обробці</option>
                    <option <?php if ($order['status'] == '2') echo 'selected' ?> value="2">Доставляється</option>
                    <option <?php if ($order['status'] == '3') echo 'selected' ?> value="3">Виконано</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Ідинтифікатор користувача</td>
            <td> <?php print($order['user_id']); ?></td>
        </tr>
        <tr>
            <td>Ідинтифікатор замовлення</td>
            <td> <?php print($order['id']); ?></td>
        </tr>
        <tr>
            <td>Ім'я</td>
            <td> <?php print($order['firstname']); ?></td>
        </tr>
        <tr>
            <td>Прізвище</td>
            <td> <?php print($order['lastname']); ?></td>
        </tr>
        <tr>
            <td>Адреса</td>
            <td> <?php print($order['address']); ?></td>
        </tr>
        <tr>
            <td>Дата замовлення</td>
            <td> <?php print($order['date']); ?></td>
        </tr>
        <tr>
            <td>Сума</td>
            <td> <?php print($order['totalPrice']); ?> грн</td>
        </tr>
        <tr>
            <td>Коментар</td>
            <td> <?php print($order['comment']); ?></td>
        </tr>
    </table>
    <h4 class="text-center">Товари</h4>
    <table class="table col-11 ml-4">
        <tr>
            <th scope="col">Іd товару</th>
            <th scope="col">Назва</th>
            <th scope="col">Ціна</th>
            <th scope="col">Кількість</th>
            <th scope="col">Повна ціна</th>
        </tr>
        <?php foreach ($products as $product) : ?>
            <tr>
                <td>
                    <?php print($product['id']) ?>
                </td>
                <td>
                    <?php print($product['name']) ?>
                </td>
                <td>
                    <?php print($product['price']) ?> грн
                </td>
                <td>
                    <?php print($countProducts[strval($product['id'])]); ?>
                </td>
                <td>
                    <?php print($countProducts[strval($product['id'])] * $product['price']); ?> грн
                </td>
            </tr>
        <?php endforeach; ?>

    </table>
</div>