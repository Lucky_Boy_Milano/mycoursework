<div class="container ">
    <a href="/admin" class="btn shadow mb-3"><i class=" fa fa-arrow-left"></i></a>
    <table class="table">
        <tr>
            <th>ID користувача</th>
            <th>ІD замовлення</t>
            <th>Ім'я</th>
            <th>Адреса</th>
            <th>Дата замовлення</th>
            <th>Сума</th>
            <th>Статус</th>
            <th></th>
            <th></th>
        </tr>
        <?php foreach ($orders as $order) : ?>
            <tr>
                <th><?php print($order['user_id']) ?></th>
                <th><?php print($order['id']) ?></th>
                <th><?php print($order['firstname']) ?></th>
                <th><?php print($order['address']) ?></th>
                <th><?php print($order['date']) ?></th>
                <th><?php print($order['totalPrice']) ?> грн</th>
                <th>
                    <p>
                        <?php print(models\Order::getStatusText($order['status'])) ?>
                    </p>
                </th>
                <th><a class="btn shadow" href="/admin/orderview?id=<?php print($order['id']) ?>"><i class="fa fa-eye" aria-hidden="true"></i></a></th>
                <th><button onclick="if (confirm('Ви дійсно хочете видалити це замовлення?'))document.location='/admin/orderdelete?id=<?php echo ($order['id']) ?>'" class="btn shadow"><i class="fa fa-trash" aria-hidden="true"></i></button></th>
            </tr>
        <?php endforeach; ?>
    </table>
</div>