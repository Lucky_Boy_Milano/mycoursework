<div class="container">
    <a href="/admin" class="btn shadow"><i class=" fa fa-arrow-left"></i></a>
    <a class="btn m-3 shadow" href="/admin/newadd"><i class="fa fa-plus" aria-hidden="true"></i></a>
    <table class="table">
        <tr>
            <th>ІD новини</th>
            <th>Заголовок</th>
            <th></th>
            <th></th>
        </tr>
        <?php foreach ($news as $new) : ?>
            <tr>
                <th><?php print($new['id']) ?></th>
                <th><?php print($new['title']) ?></th>
                <th><a href="/admin/newupdate?id=<?php echo ($new['id']) ?>" class="btn shadow"><i class="fa fa-pencil " aria-hidden="true"></i></a></th>
                <th><button onclick="if (confirm('Ви дійсно хочете видалити цю новину?'))document.location='/admin/newdelete?id=<?php echo ($new['id']) ?>'" class="btn shadow"><i class="fa fa-trash" aria-hidden="true"></i></button></th>
            </tr>
        <?php endforeach; ?>
    </table>
</div>