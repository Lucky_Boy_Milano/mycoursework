<div class="container ">
    <a href="/admin" class="btn shadow mb-3"><i class=" fa fa-arrow-left"></i></a>

    <table class="table">
        <tr>
            <th>ІD користувача</th>
            <th>Ім'я</th>
            <th>Прізвище</th>
            <th>Електронна пошта</th>
            <th>Роль</th>
            <th></th>
        </tr>
        <?php foreach ($users as $user) : ?>
            <tr>
                <th><?php print($user['id']) ?></th>
                <th><?php print($user['firstname']) ?></th>
                <th><?php print($user['lastname']) ?></th>
                <th><?php print($user['login']) ?></th>
                <th>
                    <select class="" onchange="document.location='/admin/userupdate?id=<?php echo ($user['id']) ?>&role='+ document.getElementById(<?php echo ($user['id']) ?>).value;" name="" id="<?php echo ($user['id']) ?>">
                        <option <?php if ($user['role'] == '1') echo 'selected' ?> value="1">Адмін</option>
                        <option <?php if ($user['role'] == null || $user['role'] == 0) echo 'selected' ?> value="0">Користувач</option>
                    </select>
                </th>
                <th><button onclick="if (confirm('Ви дійсно хочете видалити цього користувача?\n Ідентифікатор користувача: <?php echo ($user['id']) ?>\n'))document.location='/admin/userdelete?id=<?php echo ($user['id']) ?>'" class="btn shadow"><i class="fa fa-trash" aria-hidden="true"></i></button></th>
            </tr>
        <?php endforeach; ?>
    </table>
</div>