<div class="container col-lg-6 text-center login_form_inner register_form_inner">
    <div class="col-12 d-flex justify-justify-content-between">
        <a href="/admin/products" class="btn shadow"><i class=" fa fa-arrow-left"></i></a>
    </div>
    <div class="login-form">
        <h2 class="mb-4">Редагування товару</h2>
        <form сlass="login_form" action="/admin/productupdate?id=<?php

                                                                    use models\Product;

                                                                    echo ($product['id']); ?>" method="post" enctype="multipart/form-data">


            <table class="table">
                <tr>
                    <th> Назва товару</th>
                    <th>
                        <input class="form-control" type="text" name="name" placeholder="" value="<?php echo ($product['name']); ?>">
                    </th>
                </tr>
                <tr>
                    <th>
                        Артикул
                    </th>
                    <th>
                        <input class="form-control" type="number" name="code" placeholder="" value="<?php echo ($product['code']); ?>">
                    </th>
                </tr>
                <tr>
                    <th>Ціна</th>
                    <th>
                        <input class="form-control" type="number" name="price" placeholder="" value="<?php echo ($product['price']); ?>">
                    </th>
                </tr>
                <tr>
                    <th>Категорія</th>
                    <th>
                        <select class="" name="category_id" value="<?php echo (intval($product['category_id'])); ?>">
                            <?php if (is_array($categories)) : ?>
                                <?php foreach ($categories as $category) : ?>
                                    <option value="<?php echo ($category['id']);  ?>" <?php if ($product['category_id'] == $category['id']) echo 'selected'; ?>>
                                        <?php echo $category['name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </th>
                </tr>
                <tr>
                    <th>Додати в рекомендації</th>
                    <th> <select class="" name="is_recomended" id="">
                            <option value="1" selected>Так</option>
                            <option value="0">Ні</option>
                        </select></th>
                </tr>
                <tr>
                    <th>Відображення на сайті</th>
                    <th>
                        <select class="" name="status" id="" value="<?php echo ($product['status']); ?>">
                            <option <?php if ($product['status'] == 1) echo ('selected'); ?> value="1">Так</option>
                            <option <?php if ($product['status'] == 0) echo ('selected'); ?> value="0">Ні</option>
                        </select>
                    </th>
                </tr>
                <tr>
                    <th>Опис товару</th>
                    <th>
                        <textarea class="form-control col-11 ml-4" name="description" id="" cols="20" rows="10"><?php echo ($product['description']); ?></textarea>

                    </th>
                </tr>
                <tr>
                    <th>Зображення товару</th>
                    <th>
                        <img src="<?php echo (Product::getImage($product['id'])) ?>" alt="" class="img-thumbnail col-9 d-block ml-5">

                        <input class="form-control ml-4 col-11" type="file" name="image" value="<?php echo ($product['image']); ?>">

                    </th>
                </tr>


            </table>
            <button class="btn btn-success shadow mb-3">Редагувати <i class="fa fa-check" aria-hidden="true"></i></button>
        </form>
    </div>
</div>
</div>