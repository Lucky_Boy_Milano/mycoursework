<div class="container col-lg-6 text-center login_form_inner register_form_inner">
    <div class="col-12 d-flex justify-justify-content-between">
        <a href="/admin/news" class="btn shadow"><i class=" fa fa-arrow-left"></i></a>
    </div>
    <div class="login-form">
        <h2 class="mb-4">Редагування Новини</h2>
        <form сlass="login_form" action="/admin/newupdate?id=<?php echo($id) ?>" method="post" enctype="multipart/form-data">

            <table class="table">
                <tr>
                    <th>Заголовок</th>
                    <th><input class="form-control" type="text" name="title" value="<?php print($information['title']) ?>"></th>
                </tr>
                <tr>
                    <th>Інформація</th>
                    <th>
                        <textarea class="form-control" name="description" id="" cols="30" rows="10"><?php print($information['description']) ?></textarea>

                    </th>
                </tr>
                <tr>
                    <th>Зображення</th>
                    <th>
                        <img src="<?php echo (\models\News::getImage($information['id'])) ?>" alt="" class="img-thumbnail col-9 d-block ml-5">
                        <input class="form-control m-3" type="file" name="image">
                    </th>
                </tr>
            </table>
            <button class="btn btn-success mb-3">Змінити <i class="fa fa-check" aria-hidden="true"></i></button>
        </form>
    </div>
</div>
</div>