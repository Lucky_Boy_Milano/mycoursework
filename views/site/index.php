<main class="site-main">

    <section class="hero">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>Категорії</span>
                        </div>
                        <ul>
                            <li class=""><a href="/products/category">Всі товари</a></li>
                            <?php foreach ($categories as $category) : ?>
                                <li class=""><a href="/products/category?id=<?php echo $category['id']; ?>"><?php echo $category['name']; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form action="/products/search" method="POST">
                                <input name='search' type="text" placeholder="Що вам потрібно знайти?" value="<?php if ($search != null) echo ($search) ?>">
                                <button type="submit" class="site-btn">Пошук</button>
                            </form>
                        </div>
                        <div class="hero__search__phone">
                            <div class="hero__search__phone__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="hero__search__phone__text">
                                <h5>+380931331570</h5>
                                <span>На зв'язку 12/7</span>
                            </div>
                        </div>
                    </div>
                    <div class="hero__item set-bg" data-setbg="../../template/img/hero/Kitchen.jpg">
                        <div class="hero__text">
                            <span>СТРАВИ ПРИГОТОВАНІ ЗА ЯПОНСЬКИМИ СТАНДАРТАМИ</span>
                            <h2>Смакуйте шедеври традиційної японської кухні вже зараз</h2>
                            <p>Вибір, як капель в морі XD</p>
                            <a href="/products/" class="primary-btn">Замовляй!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="categories">
        <div class="container">
            <div class="row">
                <div class="categories__slider owl-carousel">
                    <?php foreach ($products as $product) : ?>

                        <div class="col-lg-3">
                            <div class="categories__item set-bg" data-setbg="<?php echo (\models\Product::getImage($product['id'])) ?>">
                                <h5><a href="/products/product?id=<?php echo ($product['id']) ?>"><?php print($product['name']) ?></a></h5>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>

    <section class="featured spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Страви нашого ресторану</h2>
                    </div>

                </div>
            </div>
            <div class="row featured__filter">
                <?php foreach ($products as $product) : ?>
                    <div class="col-lg-3 col-md-4 col-sm-6 mix search<?php echo ($product['category_id'] - 1) ?> fresh-meat">
                        <div class="featured__item">
                            <div class="featured__item__pic set-bg" data-setbg="<?php echo (\models\Product::getImage($product['id'])) ?>">
                                <ul class="featured__item__pic__hover">
                                    <li><a data-id="<?php echo ($product['id']) ?>" class="add-to-cart" href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                </ul>
                            </div>
                            <div class="featured__item__text">
                                <h6><a href="/products/product?id=<?php echo ($product['id']) ?>"><?php print($product['name']) ?></a></h6>
                                <h5><?php print($product['price']) ?> грн</h5>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        </div>
    </section>

    <section class="from-blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title from-blog__title">
                        <h2>Новини прямо з Китаю, тобто ЯпоніїС</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach ($news as $new) : ?>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="blog__item">
                            <div class="blog__item__pic">
                                <img src="<?php echo (\models\News::getImage($new['id'])) ?>" alt="">
                            </div>
                            <div class="blog__item__text">
                                <ul>
                                    <li><i class="fa fa-calendar-o"></i> <?php print($new['date']); ?></li>
                                </ul>
                                <h5><?php print($new['title']); ?></h5>
                                <p><?php print($new['description']); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

</main>