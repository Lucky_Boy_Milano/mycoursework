<section class="breadcrumb-section set-bg" data-setbg="../../template/img/breadcrumb.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>Замовлення</h2>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container d-flex justify-content-center">
    <div class=" col-lg-6 col-md-6">
        <div class="card border-success mt-3 ">
            <div class="card-header bg-success text-white text-center">
                <p>Замовлення <?php print($order['id']); ?></p>
            </div>
            <div class="card-text">
                <table class="table table-borderless">
                    <tr>
                        <th>Ім'я:</th>
                        <th>
                            <p>
                                <?php print($order['firstname']); ?>
                            </p>
                        </th>
                    </tr>
                    <tr>
                        <th>Прізвище:</th>
                        <th>
                            <p><?php print($order['lastname']); ?></p>
                        </th>
                    </tr>
                    <tr>
                        <th>Адреса:</th>
                        <th>
                            <p><?php print($order['address']); ?></p>
                        </th>
                    </tr>
                    <tr>
                        <th>Дата замовлення:</th>
                        <th>
                            <p><?php print($order['date']); ?></p>
                        </th>
                    </tr>
                    <tr>
                        <th>Сума:</th>
                        <th>
                            <p><?php print($order['totalPrice']); ?> грн</p>
                        </th>
                    </tr>
                    <tr>
                        <th>Статус замовлення:</th>
                        <th>
                            <p><?php print models\Order::getStatusText($order['status']) ?></p>
                        </th>
                    </tr>
                    <tr>
                        <th>Коментар:</th>
                        <th>
                            <p><?php print($order['comment']); ?></p>
                        </th>
                    </tr>
                </table>
                <div class="col-12 text-center">
                    <a href="/profile" class="p-2 btn-success text-center">Повернутися до профілю</a>
                </div>
            </div>
        </div>
    </div>
</div>

<h4 class="text-center mt-4 mb-2 text-success">Товари</h4>
<table class="table col-12 table-success">
    <tr>
        <th scope="col">Іd товару</th>
        <th scope="col">Назва</th>
        <th scope="col">Ціна</th>
        <th scope="col">Кількість</th>
        <th scope="col">Повна ціна</th>
    </tr>
    <?php foreach ($products as $product) : ?>
        <tr>
            <td>
                <?php print($product['id']) ?>
            </td>
            <td>
                <?php print($product['name']) ?>
            </td>
            <td>
                <?php print($product['price']) ?> грн
            </td>
            <td>
                <?php print($countProducts[strval($product['id'])]); ?>
            </td>
            <td>
                <?php print($countProducts[strval($product['id'])] * $product['price']); ?> грн
            </td>
        </tr>
    <?php endforeach; ?>

</table>