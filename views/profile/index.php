    <section class="breadcrumb-section set-bg" data-setbg="../../template/img/breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Кабінет користувача</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container col-lg-9 col-md-9 col-sm-12 login_form_inner register_form_inner">
        <?php if (isset($edited)) : ?>
            <div class="text-center alert alert-success alert-dismissible fade show col-12 mt-3" role="alert">
                Дані успішно змінено
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endif; ?>

        <br>
        <div class="section-intro pb-60px">
            <div class="container m-2 mt-4">
                <?php if ($role == 1) : ?>
                <div>
                    <p >Ви можете використувовути можливості адміністратора.</p>

                    <a class="p-2 btn-success d-inline-block" href="/admin">Адміністатор <i class="fa fa-address-card-o" aria-hidden="true"></i></a>
                </div>
                    <?php endif; ?>
                <p>Доброго дня, <?php print($firstname) ?> <?php print($lastname) ?>.</p>
                <p>Ми раді вас бачити.</p>
                <p>Ви використовуєте електронну пошту : <?php print($login) ?>.</p>
                <div>
                    <a href="/profile/edit" class="p-2 btn-success d-inline-block">Редагувати дані <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                    <a href="/users/logout" class="p-2 btn-danger d-inline-block">Вийти <i class="fa fa-sign-out" aria-hidden="true"></i></i></a>
                </div>
            </div>
        </div>
        <div class="section-intro text-center">
            <h3><span class="section-intro__style ">Ваші замовлення</span></h3>
            <div class="container">
                <?php foreach ($orders as $order) : ?>

                    <table class="table table-borderless border border-success">
                        <tr>
                            <th>ІD замовлення</th>
                            <th>Сума</th>
                            <th>Дата</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        <tr>
                            <th><?php print($order['id']) ?></th>
                            <th><?php print($order['totalPrice']) ?> грн</th>
                            <th><?php print($order['date']) ?></th>
                            <th>
                                <?php print models\Order::getStatusText($order['status']) ?>
                            </th>
                            <th><a class="primary-btn float-right" href="/profile/view?id=<?php print($order['id']) ?>">Перегляд</a></th>
                        </tr>

                    </table>
                <?php endforeach; ?>
            </div>
        </div>
    </div>