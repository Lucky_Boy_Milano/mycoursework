    <section class="breadcrumb-section set-bg" data-setbg="../../template/img/breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Редагування</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="d-flex justify-content-center ">
        <div class="col-lg-6 col-md-9 col-sm-12 mt-3">
            <form class="row text-success" method="POST" action="/profile/edit" id="register_form">
                <div class="col-md-12 form-group">
                    <label class="col-12 ">Ім'я:
                        <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Ім'я" value="<?php echo ($firstname) ?>">
                    </label>
                </div>

                <div class="col-md-12 form-group">
                    <label class="col-12">Прізвище:
                        <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Прізвище" value="<?php echo ($lastname) ?>">
                    </label>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-12">Електронна пошта:
                        <input type="text" class="form-control" id="login" name="login" placeholder="Електронна пошта" value="<?php echo ($login) ?>">
                    </label>
                </div>
                <div class="col-md-12 form-group">
                    <label class="col-12">Пароль:
                        <input type="text" class="form-control" id="password" name="password" placeholder="Пароль" value="<?php echo ($password) ?>">
                    </label>
                </div>
                <?php foreach ($errors as $error) : ?>
                    <div class="alert alert-success alert-dismissible fade show col-11 ml-4" role="alert">
                        <?php print($error); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php endforeach; ?>
                <div class="col-md-12 form-group">
                    <button type="submit" value="submit" class="btn btn-success w-100">Редагувати</button>
                </div>
            </form>
        </div>
    </div>