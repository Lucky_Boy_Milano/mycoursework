<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $Title ?></title>


    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="../../template/img/icon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../../template/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../../template/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../../template/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="../../template/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="../../template/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="../../template/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../../template/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../../template/css/style.css" type="text/css">
</head>

<body>
<script src="../../template/js/js.js"></script>

    <div class="humberger__menu__overlay"></div>
    <div class="humberger__menu__wrapper">
        <div class="humberger__menu__logo">
            <a href="#"><img src="../../template/img/logoGreen.png" alt=""></a>
        </div>
        <div class="humberger__menu__cart">
            <ul>
                <li><a href="#"><i class="fa fa-shopping-bag"></i> <span><?php print(models\Cart::countItems()); ?></span></a></li>
            </ul>
        </div>
        <div class="humberger__menu__widget">
            <div class="header__top__right__auth">
                <a href="/profile"><i class="fa fa-user"></i> <?php if (models\Users::isGuest()) {
                                                                    print('Вхід');
                                                                } else {
                                                                    print('Профіль');
                                                                } ?></a>
            </div>
        </div>
        <nav class="humberger__menu__nav mobile-menu">
            <ul>
                <li><a href="/">Головна сторінка</a></li>
                <li><a href="/products/category">Товари</a></li>
                <li><a href="/news">Новини</a></li>
            </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        <div class="header__top__right__social">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-pinterest-p"></i></a>
        </div>
        <div class="humberger__menu__contact">
            <ul>
                <li><i class="fa fa-envelope"></i> japan_cuisin@ukr.net</li>
                <li>Широкий асортимент та швидка доставка. Все для Вас ;)</li>
            </ul>
        </div>
    </div>

    <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-7">
                        <div class="header__top__left">
                            <ul>
                                <li><i class="fa fa-envelope"></i> japan_cuisin@ukr.net</li>
                                <li>Широкий асортимент та швидка доставка. Все для Вас ;)</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5">
                        <div class="header__top__right">
                            <div class="header__top__right__social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                            <div class="header__top__right__auth">
                                <a href="/profile"><i class="fa fa-user"></i> <?php if (models\Users::isGuest()) {
                                                                                    print('Вхід');
                                                                                } else {
                                                                                    print('Профіль');
                                                                                } ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="/"><img src="../../template/img/Sushi.jpg" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <nav class="header__menu">
                        <ul>
                            <li><a href="/">Головна</a></li>
                            <li><a href="/products/category">Товари</a></li>
                            <li><a href="/news">Новини</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3">
                    <div class="header__cart">
                        <ul>
                            <li><a href="/cart"><i class="fa fa-shopping-bag"></i> <span class="cart-count"><?php print(models\Cart::countItems()); ?></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>

    <?= $Content ?>

    <footer class="footer spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__about__logo">
                            <a href="./index.html"><img src="img/logo.png" alt=""></a>
                        </div>
                        <ul>
                            <li>Адреса: місто Житомир, вулиця Бердичівська, 10</li>
                            <li>Номер телефону: +380931331325</li>
                            <li>Електронна пошта: pi59_kdm@student.ztu.edu.ua</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
                    <div class="footer__widget">
                        <h6>Меню</h6>
                        <ul>
                            <li><a href="/">Головна сторінка</a></li>
                            <li><a href="/products">Товари</a></li>
                            <li><a href="/news">Новини</a></li>
                            <li><a href="/profile">Профіль</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="footer__widget">
                        <h6>Підписка</h6>
                        <p>Підпишіться на нас, щоб отримати останні новини.</p>
                        <form action="/site/subscribe" method="post">
                            <input name="email" type="text">
                            <button type="submit" class="site-btn">Підписатись</button>
                            </form>
                            <div class="footer__widget__social">
                            <a ><i class="fa fa-facebook"></i></a>
                            <a ><i class="fa fa-instagram"></i></a>
                            <a ><i class="fa fa-twitter"></i></a>
                            <a ><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__copyright">
                        <div class="footer__copyright__text">
                            <p>
                                Здорова їжа &copy;<script>
                                    document.write(new Date().getFullYear());
                                </script> Все для вас | Приємних покупок <i class="fa fa-heart" aria-hidden="true"></i> by <a>Denys_Kovtunovych Inc.</a>
                            </p>
                        </div>
                        <div class="footer__copyright__payment"><img src="../../template/img/payment-item.png" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="../../template/js/jquery-3.3.1.min.js"></script>
    <script src="../../template/js/bootstrap.min.js"></script>
    <script src="../../template/js/jquery.nice-select.min.js"></script>
    <script src="../../template/js/jquery-ui.min.js"></script>
    <script src="../../template/js/jquery.slicknav.js"></script>
    <script src="../../template/js/mixitup.min.js"></script>
    <script src="../../template/js/owl.carousel.min.js"></script>
    <script src="../../template/js/js.js"></script>


    <script>
        $(document).ready(function() {
            $(".add-to-cart").click(function() {
                var id = $(this).attr("data-id");
                $.post("/cart/addajax?id=" + id, {},
                    function(data) {
                        var i = data.match("<span class=\"cart-count\">([0-9]+)</span>")[1];
                        $(".cart-count").html(i);
                    });
                return false;
            });
        });
    </script>
</body>

</html>