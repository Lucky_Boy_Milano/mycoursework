
   <section class="breadcrumb-section set-bg" data-setbg="../../template/img/breadcrumb.jpg">
       <div class="container">
           <div class="row">
               <div class="col-lg-12 text-center">
                   <div class="breadcrumb__text">
                       <h2>Реєстрація</h2>
                   </div>
               </div>
           </div>
       </div>
       </div>
   </section>

   <section class="">
       <div class="container">
           <div class="row d-flex justify-content-center">
               <div class="col-lg-6 m-3">
                   <div class="login_form_inner register_form_inner">
                       <form class="row login_form" method="POST" action="/users/register" id="register_form">
                           <div class="col-md-12 form-group">
                               <label class="col-12"> Ім'я
                                   <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Ім'я" value="<?php echo ($post['firstname']) ?>">
                               </label>
                           </div>
                           <div class="col-md-12 form-group">
                               <label class="col-12"> Прізвище
                                   <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Прізвище" value="<?php echo ($post['lastname']) ?>">
                               </label>
                           </div>
                           <div class="col-md-12 form-group">
                               <label class="col-12"> Електронна пошта
                                   <input type="text" class="form-control" id="login" name="login" placeholder="Електронна пошта" value="<?php echo ($post['login']) ?>">
                               </label>
                           </div>
                           <div class="col-md-12 form-group">
                               <label class="col-12"> Пароль
                                   <input type="text" class="form-control" id="password" name="password" placeholder="Пароль" value="<?php echo ($post['password']) ?>">
                               </label>
                           </div>
                           <div class="col-md-12 form-group">
                               <label class="col-12"> Повторіть пароль
                                   <input type="text" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Повторіть пароль">
                               </label>
                           </div>
                           <?php foreach ($errors as $error) : ?>
                               <div class="alert alert-danger alert-dismissible fade show col-12" role="alert">
                                   <?php print($error); ?>
                                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                   </button>
                               </div>
                           <?php endforeach; ?>
                           <div class="col-md-12 form-group d-flex justify-content-center">
                               <button type="submit" value="submit" class="btn btn-success w-50">Зареєструватись</button>
                           </div>
                           <div>
                               <p>
                                   Ви вже маєте обліковий запис? Натисніть -
                                   <a class="btn-link text-success" href="/users/login">Увійти</a>
                               </p>
                           </div>
                       </form>
                   </div>
               </div>
           </div>
       </div>
   </section>
