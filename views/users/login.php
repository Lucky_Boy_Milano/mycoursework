
   <section class="breadcrumb-section set-bg" data-setbg="../../template/img/breadcrumb.jpg">
       <div class="container">
           <div class="row">
               <div class="col-lg-12 text-center">
                   <div class="breadcrumb__text">
                       <h2>Авторизація</h2>
                   </div>
               </div>
           </div>
       </div>
       </div>
   </section>

   <section class="section-margin">
       <div class="container d-flex justify-content-center">
           <div class="col-lg-6 m-3">
               <div class="login_form_inner">
                   <form class="row login_form" method="POST" action="/users/login" id="contactForm">
                       <div class="col-md-12 form-group">
                           <label class="col-12"> Логін
                               <input type="email" class="form-control" id="login" name="login" placeholder="Електронна адреса" value="<?php echo ($login) ?>">
                           </label>
                       </div>

                       <div class=" col-md-12 form-group">
                           <label class="col-12">Пароль
                               <input type="password" class="form-control" id="password" name="password" placeholder="Пароль">
                           </label>
                       </div>
                       <?php foreach ($errors as $error) : ?>
                           <div class="alert alert-danger alert-dismissible fade show col-12" role="alert">
                               <?php print($error); ?>
                               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                               </button>
                           </div>
                       <?php endforeach; ?>
                       <div class="col-md-12 form-group d-flex justify-content-center">
                           <button type="submit" value="submit" class="btn btn-success w-50">Увійти</button>
                       </div>
                       <div>
                           <h5 class="m-3"> Якщо ви ще не зареєстровані то
                               <a class="btn-link text-success" href="/users/register">Створіть обліковий запис</a>
                           </h5>
                       </div>
                   </form>
               </div>
           </div>
       </div>
       </div>
   </section>
