   <section class="breadcrumb-section set-bg" data-setbg="../../template/img/breadcrumb.jpg">
       <div class="container">
           <div class="row">
               <div class="col-lg-12 text-center">
                   <div class="breadcrumb__text">
                       <h2>Новини</h2>
                   </div>
               </div>
           </div>
       </div>
   </section>

   <section class="blog spad">
       <div class="container d-flex justify-content-center">
           <div class="col-lg-12 col-md-12">
               <div class="row">
                   <?php foreach ($news as $new) : ?>
                       <div class="col-lg-4 col-md-6 col-sm-12">
                           <div class="blog__item">
                               <div class="blog__item__pic">
                                   <img src="<?php echo (\models\News::getImage($new['id'])) ?>" alt="">
                               </div>
                               <div class="blog__item__text">
                                   <ul>
                                       <li><i class="fa fa-calendar-o"></i><?php print($new['date']); ?></li>
                                   </ul>
                                   <h5>
                                       <p><?php print($new['title']); ?></p>
                                   </h5>
                                   <p><?php print($new['description']); ?></p>
                               </div>
                           </div>
                       </div>
                   <?php endforeach; ?>
               </div>
           </div>
       </div>

       <nav class="blog-pagination justify-content-center d-flex">
           <?php echo ($pagination->get()) ?>

       </nav>
       </div>
   </section>
