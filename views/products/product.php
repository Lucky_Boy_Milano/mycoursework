    <section class="breadcrumb-section set-bg" data-setbg="../../template/img/breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2><?php print($category) ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="product-details spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="product__details__pic">
                        <div class="product__details__pic__item">
                            <img class="product__details__pic__item--large" src="<?php echo (\models\Product::getImage($product['id'])) ?>" alt="">
                        </div>
                        <div class="product__details__pic__slider owl-carousel">
                            <img data-imgbigurl="img/product/details/product-details-2.jpg" src="img/product/details/thumb-1.jpg" alt="">
                            <img data-imgbigurl="img/product/details/product-details-3.jpg" src="img/product/details/thumb-2.jpg" alt="">
                            <img data-imgbigurl="img/product/details/product-details-5.jpg" src="img/product/details/thumb-3.jpg" alt="">
                            <img data-imgbigurl="img/product/details/product-details-4.jpg" src="img/product/details/thumb-4.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="product__details__text">
                        <h3><?php print($product['name']) ?></h3>

                        <div class="product__details__price"><?php print($product['price']) ?> грн</div>
                        <p><?php print($product['description']) ?></p>
                        <a href="#" class="primary-btn add-to-cart" data-id="<?php echo ($product['id']) ?>">Додати в корзину</a>
                        <ul>
                            <li><b>Категорія</b> <span><a class="btn" href="/products/category?id=<?php print($product['category_id']) ?>"><?php print($category) ?></a></span></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
