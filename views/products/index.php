  <section class="hero hero-normal">
      <div class="container">
          <div class="row">
              <div class="col-lg-3">

              </div>
              <div class="col-lg-9">
                  <div class="hero__search">
                      <div class="hero__search__form">
                          <form action="/products/search" method="POST">
                              <input name='search' type="text" placeholder="Що вам потрібно знайти?" value="<?php if ($search != null) echo ($search) ?>">
                              <button type="submit" class="site-btn">Пошук</button>
                          </form>
                      </div>
                      <div class="hero__search__phone">
                          <div class="hero__search__phone__icon">
                              <i class="fa fa-phone"></i>
                          </div>
                          <div class="hero__search__phone__text">
                              <h5>+38055512345</h5>
                              <span>Підтримка 24/7</span>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>

  <section class="breadcrumb-section set-bg" data-setbg="../../template/img/breadcrumb.jpg">
      <div class="container">
          <div class="row">
              <div class="col-lg-12 text-center">
                  <div class="breadcrumb__text">
                      <h2>Товари</h2>
                  </div>
              </div>
          </div>
      </div>
  </section>

  <section class="product spad">
      <div class="container">
          <div class="row">
              <div class="col-lg-3 col-md-5">
                  <div class="sidebar">
                      <div class="sidebar__item">
                          <h4>Категорії</h4>
                          <ul>
                              <li class=""><a class="<?php if ($id == 0) echo ('text-success'); ?>" href="/products/category">Всі товари</a></li>
                              <?php foreach ($categories as $category) : ?>
                                  <li><a class="<?php if ($id == $category['id']) echo ('text-success'); ?>" href="/products/category?id=<?php echo $category['id']; ?>"><?php echo $category['name']; ?></a></li>
                              <?php endforeach; ?>
                          </ul>
                      </div>
                  </div>
              </div>
              <div class="col-lg-9 col-md-7">
                  <div class="row">
                      <?php foreach ($products as $product) : ?>
                          <div class="col-lg-4 col-md-6 col-sm-6">
                              <div class="product__item">
                                  <div class="product__item__pic set-bg" data-setbg="<?php echo (\models\Product::getImage($product['id'])); ?>">
                                      <ul class="product__item__pic__hover">
                                          <li><a class="add-to-cart" href="#" data-id="<?php echo ($product['id']) ?>"><i class="fa fa-shopping-cart"></i></a></li>
                                      </ul>
                                  </div>
                                  <div class="product__item__text">
                                      <p><?php print($categories[$product['category_id']]['name']) ?></p>

                                      <h6><a href="/products/product?id=<?php echo ($product['id']) ?>"><?php print($product['name']) ?></a></h6>
                                      <h5><?php print($product['price']) ?> грн</h5>
                                  </div>
                              </div>
                          </div>
                      <?php endforeach; ?>
                  </div>
                  <div class="ml-5">
                      <?php if ($pagination != 0) echo ($pagination->get()) ?>
                  </div>
              </div>
          </div>
      </div>
      </div>
  </section>
