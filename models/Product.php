<?php

namespace models;

class Product
{
    protected $limitProductsPage = 9;

    public function getAllProducts($page)
    {
        global $core;
        $numberProduct = ($page - 1) * $this->limitProductsPage;
        $query = new \core\DBQuery('products');
        $res = $core->getDB()->executeQuery($query->select('*')->where(['status' => 1])->limit($this->limitProductsPage)->offset($numberProduct));

        return $res;
    }

    public function getRecomendedProducts()
    {
        global $core;
        $query = new \core\DBQuery('products');
        $res = $core->getDB()->executeQuery($query->select('*')->where(['is_recomended' => 1]));
        return $res;
    }

    public function getProductsList()
    {
        global $core;
        $query = new \core\DBQuery('products');
        $res = $core->getDB()->executeQuery($query->select('*'));
        return $res;
    }

    public function deleteProductById($id)
    {
        global $core;
        $query = new \core\DBQuery('products');
        $res = $core->getDB()->executeQuery($query->delete('*')->where(['id' => $id]));
        return $res;
    }

    public function getProductsByCategory($category_id, $page)
    {
        global $core;
        $numberProduct = ($page - 1) * $this->limitProductsPage;
        $query = new \core\DBQuery('products');
        $res = $core->getDB()->executeQuery($query->select('*')->where(['status' => 1, 'category_id' => $category_id])->limit($this->limitProductsPage)->offset($numberProduct));
        return $res;
    }

    public function getProductsByIds($idsArray)
    {
        global $core;
        $products = [];

        $query = new \core\DBQuery('products');
        $res = $core->getDB()->executeQuery($query->select('*')->where(['status' => 1]));

        foreach ($res as $item) {
            if (in_array($item['id'], $idsArray))
                $products[] = $item;
        }
        return $products;
    }


    public function getProductsByName($name)
    {
        global $core;
        $query = new \core\DBQuery('products');
        $res = $core->getDB()->executeQuery($query->select('*')->like(['name' => $name]));
        return $res;
    }

    public function getProductById($id)
    {
        global $core;
        $query = new \core\DBQuery('products');
        $res = $core->getDB()->executeQuery($query->select('*')->where(['id' => $id])->one());
        return $res;
    }

    public function getProductInfo($product_id)
    {
        global $core;
        $query = new \core\DBQuery('products');
        $res = $core->getDB()->executeQuery($query->select('*')->where(['status' => 1, 'id' => $product_id])->one());
        return $res;
    }

    public function updateProduct($id, $options)
    {
        global $core;
        $query = new \core\DBQuery('products');
        $core->getDB()->executeQuery($query->update(
            [
                'name' => $options['name'],
                'category_id' => $options['category_id'],
                'code' => $options['code'],
                'price' => $options['price'],
                'description' => $options['description'],
                'is_recomended' => $options['is_recomended'],
                'status' => $options['status']

            ]
        )->where(['id' => $id]));
    }


    public function getCountProductsByCategory($id)
    {
        global $core;
        $query = new \core\DBQuery('products');
        if ($id == null) {
            return $core->getDB()->executeQuery($query->select('COUNT(*) as count')->one());
        } else {
            return $core->getDB()->executeQuery($query->select('COUNT(*) as count')->where(['category_id' => $id])->one());
        }
    }

    public function createProduct($options)
    {
        global $core;
        $query = new \core\DBQuery('products');
        $core->getDB()->executeQuery($query->insert(
            [
                'name' => $options['name'],
                'category_id' => $options['category_id'],
                'code' => $options['code'],
                'price' => $options['price'],
                'description' => $options['description'],
                'is_recomended' => $options['is_recomended'],
                'status' => $options['status']

            ]
        ));

        $res = $this->getLastAddProduct();
        return $res;
    }

    public static function getImage($id)
    {
        $noImage = "no-image.jpg";

        $path = '/upload/images/products/';

        $pathImage = $path . strval($id) . '.jpg';

        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $pathImage)) {
            return $pathImage;
        }
        return $path . $noImage;
    }

    public function getLastAddProduct()
    {
        global $core;
        $query = new \core\DBQuery('products');
        $count = $this->getCountProducts();
        return $core->getDB()->executeQuery($query->select('id')->limit(1)->offset(intval($count) - 1)->one());
    }

    public function getCountProducts()
    {
        global $core;
        $query = new \core\DBQuery('products');
        return $core->getDB()->executeQuery($query->select('COUNT(*) as count')->one())['count'];
    }
}
