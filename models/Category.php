<?php

namespace models;

class Category
{
    public function getCategoriesList()
    {
        global $core;
        $result = [];
        $query = new \core\DBQuery('category');
        $res = $core->getDB()->executeQuery($query->select('name, id'));
        foreach ($res as $item) {
            $result[$item['id']]['name'] = $item['name'];
            $result[$item['id']]['id'] = $item['id'];
        }
        return $result;
    }

    public function deleteCategoryById($id)
    {
        global $core;
        $query = new \core\DBQuery('category');
        $core->getDB()->executeQuery($query->delete('*')->where(['id' => $id]));
    }

    public function addCategory($options)
    {
        global $core;
        $query = new \core\DBQuery('category');
        $core->getDB()->executeQuery($query->insert(['name' => $options['name']]));
    }
}
