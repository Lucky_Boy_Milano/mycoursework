<?php

namespace models;

class News extends \core\Model
{
    protected $ModelPagination;

    public function getNews($limitNewsPage,$page)
    {
        if ($page == null) $page = 1;
        global $core;
        $numberNews = ($page - 1) * $limitNewsPage;
        $query = new \core\DBQuery('news');
        $res = $core->getDB()->executeQuery($query->select('*')->limit($limitNewsPage)->offset($numberNews)->orderBy('-id'));
        return $res;
    }
    public function getCountNews()
    {
        global $core;
        $query = new \core\DBQuery('news');
        return $core->getDB()->executeQuery($query->select('COUNT(*) as count')->one())['count'];
    }
    public function getAllNews()
    {
        global $core;
        $query = new \core\DBQuery('news');
        $res = $core->getDB()->executeQuery($query->select('*'));
        return $res;
    }
    public function createNews($options)
    {
        global $core;
        $query = new \core\DBQuery('news');
        $core->getDB()->executeQuery($query->insert(
            [
                'title' => $options['title'],
                'description' => $options['description'],
                'date' => date("d.m.Y h:i")
            ]
        ));

        $res = $this->getLastAddNew();
        return $res;
    }

    public function updateNew($id, $options)
    {
        global $core;
        $query = new \core\DBQuery('news');
        $core->getDB()->executeQuery($query->update(
            [
                'title' => $options['title'],
                'description' => $options['description'],
            ]
        )->where(['id' => $id]));
    }
    public function getNewsById($id)
    {
        global $core;
        $query = new \core\DBQuery('news');
        $res = $core->getDB()->executeQuery($query->select('*')->where(['id' => $id])->one());
        return $res;
    }
    public static function getImage($id)
    {
        $noImage = "no-image.jpg";

        $path = '/upload/images/news/';

        $pathImage = $path . strval($id) . '.jpg';

        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $pathImage)) {
            return $pathImage;
        }
        return $path . $noImage;
    }
    public function getLastAddNew()
    {
        global $core;
        $query = new \core\DBQuery('news');
        $count = $this->getCountNews();
        return $core->getDB()->executeQuery($query->select('id')->limit(1)->offset(intval($count) - 1)->one());
    }

    public function deleteNewById($id)
    {
        global $core;
        $query = new \core\DBQuery('news');
        $res = $core->getDB()->executeQuery($query->delete('*')->where(['id' => $id]));
        return $res;
    }
}
