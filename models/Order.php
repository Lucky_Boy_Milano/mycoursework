<?php

namespace models;

class Order extends \core\Model
{
    public function createOrder($user_id = 0, $firstname, $lastname, $address, $comment, $products, $totalPrice)
    {
        global $core;
        $query = new \core\DBQuery('orders');
        $products = json_encode($products);
        $core->getDB()->executeQuery($query->insert(
            [
                'user_id' => $user_id,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'address' => $address,
                'comment' => $comment,
                'products' => $products,
                'totalPrice' => $totalPrice,
                'status' => 0,
                'date' => date("F j, Y, g:i a")
            ]
        ));
    }

    public function getAllOrders()
    {
        global $core;
        $query = new \core\DBQuery('orders');
        return $core->getDB()->executeQuery($query->select("*"));
    }

    public function getOrdersById($id)
    {
        global $core;
        $query = new \core\DBQuery('orders');
        return $core->getDB()->executeQuery($query->select("*")->where(['user_id' => $id]));
    }

    public function getOrderById($id)
    {
        global $core;
        $query = new \core\DBQuery('orders');
        return $core->getDB()->executeQuery($query->select("*")->where(['id' => $id])->one());
    }

    public function changeStatus($id, $status)
    {
        global $core;
        $query = new \core\DBQuery('orders');
        return $core->getDB()->executeQuery($query->update(['status' => $status])->where(['id' => $id]));
    }

    public function deleteOrderById($id)
    {
        global $core;
        $query = new \core\DBQuery('orders');
        $res = $core->getDB()->executeQuery($query->delete('*')->where(['id' => $id]));
        return $res;
    }

    public static function getStatusText($status)
    {
        switch ($status) {
            case '0':
                return 'Нове замовлення';
                break;
            case '1':
                return 'В обробці';
                break;
            case '2':
                return 'Доставляється';
                break;
            case '3':
                return 'Виконано';
                break;
        }
    }
}
