<?php

namespace models;

class Subscribe extends \core\Model
{
    public function addSubscribe($email)
    {
        global $core;
        $query = new \core\DBQuery('subscribe');
        $core->getDB()->executeQuery($query->insert(['email' => $email]));
    }

    public function deleteSubscribe($id)
    {
        global $core;
        $query = new \core\DBQuery('subscribe');
        $core->getDB()->executeQuery($query->delete('*')->where(['id' => $id]));
    }

    public function getAllSubscribe()
    {
        global $core;
        $query = new \core\DBQuery('subscribe');
        return $core->getDB()->executeQuery($query->select('*'));
    }
}
