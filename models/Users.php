<?php

namespace models;

class Users extends \core\Model
{
    public function __construct()
    {
    }
    /**
     * Метод авторизації
     */
    public function Authenticate($login, $password)
    {
        global $core;
        $query = new \core\DBQuery('users');
        $res = $core->getDB()->executeQuery($query->select('id')
            ->where(['login' => $login, 'password' => $password])->one());
        return $res['id'];
    }
    /**
     * Метод реєстрації
     */
    public function Register($login, $password, $firstname, $lastname)
    {
        global $core;
        $query = new \core\DBQuery('users');
        $array = array(
            'login' => $login,
            'password' => $password,
            'firstname' => $firstname,
            'lastname' => $lastname
        );
        $core->getDB()->executeQuery($query->insert($array));
    }
    /**
     * Метод оновлення даних
     */
    public function Edit($login, $password, $firstname, $lastname)
    {
        global $core;
        $query = new \core\DBQuery('users');
        $array = array(
            'login' => $login,
            'password' => $password,
            'firstname' => $firstname,
            'lastname' => $lastname
        );
        $core->getDB()->executeQuery($query->update($array)->where(['login' => $login]));
    }
    /**
     * Записуємо користувача в сесію
     */
    public function auth($userId)
    {
        $_SESSION['user'] = $userId;
    }
    /**
     * Перевірка на авторизацію користувача
     */
    public function checkLogged()
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        } else {
            header("Location: /users/login");
        }
    }
    /**
     * Перевірка на неавторизацію користувача
     */
    public static function isGuest()
    {
        if (isset($_SESSION['user']))
            return false;
        else
            return true;
    }
    /**
     * Отримуємо користувача за його id
     */
    public function getUserById($id)
    {
        global $core;
        $query = new \core\DBQuery('users');
        $res = $core->getDB()->executeQuery($query->select('*')
            ->where(['id' => $id])->one());
        return $res;
    }
    public function checkEmailExists($login)
    {
        global $core;
        $query = new \core\DBQuery('users');
        $res = $core->getDB()->executeQuery($query->select('COUNT(*) as count')
            ->where(['login' => $login])->one());
        return $res['count'] != 0;
    }

    public function checkNameLength($name)
    {
        if (strlen($name) > 2)
            return true;
        else
            return false;
    }

    public function checkPasswordLength($password)
    {
        if (strlen($password) > 6)
            return true;
        else
            return false;
    }

    public function checkConfirmPassword($password, $confirmPassword)
    {
        if ($password == $confirmPassword)
            return true;
        else
            return false;
    }

    public function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL))
            return true;
        else
            return false;
    }

    public function getAllUsers()
    {
        global $core;
        $query = new \core\DBQuery('users');
        return $core->getDB()->executeQuery($query->select('*'));
    }

    public function deleteUserById($id)
    {
        global $core;
        $query = new \core\DBQuery('users');
        $core->getDB()->executeQuery($query->delete('*')->where(['id' => $id]));
    }

    public function updateUserRole($id, $roleId)
    {
        global $core;
        $query = new \core\DBQuery('users');
        $core->getDB()->executeQuery($query->update(['role' => $roleId])->where(['id' => $id]));
    }
}
