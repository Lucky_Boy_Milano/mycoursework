<?php

namespace models;

class Cart extends \core\Model
{
    /**
     * Добавление товара в корзину
     */
    public function addProduct($id)
    {
        $id = intval($id);

        $productsInCart = [];

        if (isset($_SESSION['products'])) {
            $productsInCart = $_SESSION['products'];
        }

        if (array_key_exists($id, $productsInCart)) {
            $productsInCart[$id]++;
        } else {
            $productsInCart[$id] = 1;
        }


        $_SESSION['products'] = $productsInCart;

        return self::countItems();
    }

    public function changeCount($id, $count)
    {
        $id = intval($id);
        if ($count > 0) {
            $_SESSION['products'][$id] = $count;
        } else {
            // $mas = $_SESSION['products'];
            // unset($mas[$id]);
            unset($_SESSION['products'][$id]);
        }
    }

    public static function countItems()
    {
        if (isset($_SESSION['products'])) {
            $count = 0;
            foreach ($_SESSION['products'] as $id => $quantity) {
                $count = $count + $quantity;
            }
            return $count;
        } else {
            return 0;
        }
    }
    public function getProducts()
    {
        if (isset($_SESSION['products'])) {
            return $_SESSION['products'];
        }
        return [];
    }
    public function getTotalPrice($products)
    {
        $productsInCart = $this->getProducts();

        $total = 0;

        if ($productsInCart) {
            foreach ($products as $item) {
                $total += $item['price'] * $productsInCart[$item['id']];
            }
        }

        return $total;
    }
    public function clearCart()
    {
        unset($_SESSION['products']);
    }
}
