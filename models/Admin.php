<?php

namespace models;

class Admin extends \core\Model
{
    protected $ModelUser;
    public function __construct()
    {
        $this->ModelUser = new \models\Users();
    }
    public function checkAdmin()
    {

        $userId = $this->ModelUser->checkLogged();

        $user = $this->ModelUser->getUserById($userId);

        if ($user['role'] == 1) {
            return true;
        }

        die('Ви не маєте доступу до цієї сторінки');
    }

}
