<?php

namespace models;
/*
 * Класс для генерування сторінкової навігації
 */

class Pagination
{

    private $max = 10;
    private $index = 'page';
    private $current_page;
    private $total;
    private $limit;
    public function __construct($total, $currentPage, $limit, $index)
    {
        $this->total = $total;

        $this->limit = $limit;

        $this->index = $index;

        $this->amount = $this->amount();

        $this->setCurrentPage($currentPage);
    }

    public function get()
    {
        $links = null;

        $limits = $this->limits();

        $html = '<div class="product__pagination">';
        for ($page = $limits[0]; $page <= $limits[1]; $page++) {
            if ($page == $this->current_page) {
                $links .= '<a class="text-warning"  href="#">' . $page . '</a>';
            } else {
                $links .= $this->generateHtml($page);
            }
        }

        if (!is_null($links)) {
            if ($this->current_page > 1)
                $links = $this->generateHtml(1, '&lt;') . $links;

            if ($this->current_page < $this->amount)
                $links .= $this->generateHtml($this->amount, '&gt;');
        }

        $html .= $links . '</div>';
        return $html;
    }

    private function generateHtml($page, $text = null)
    {
        if (!$text)
            $text = $page;

        $currentURI = rtrim($_SERVER['REQUEST_URI'], '?') . '?';
        $currentURI = preg_replace('~/?page=[0-9]+~', '', $currentURI);
        if (substr($currentURI, -2) == '??' || substr($currentURI, -2) == '&?') {
            $currentURI = mb_substr($currentURI, 0, -1);
        }
        $value = mb_substr(substr($currentURI, -2), 0, -1);
        if (is_numeric($value) && substr($currentURI, -1) == '?') {
            $currentURI = mb_substr($currentURI, 0, -1);
            $currentURI = $currentURI . '&';
        }
        return
            '<a href="' . $currentURI . $this->index . $page . '">' . $text . '</a>';
    }

    private function limits()
    {
        $left = $this->current_page - round($this->max / 2);

        $start = $left > 0 ? $left : 1;

        if ($start + $this->max <= $this->amount)
            $end = $start > 1 ? $start + $this->max : $this->max;
        else {
            $end = $this->amount;

            $start = $this->amount - $this->max > 0 ? $this->amount - $this->max : 1;
        }

        return
            array($start, $end);
    }

    private function setCurrentPage($currentPage)
    {
        $this->current_page = $currentPage;

        if ($this->current_page > 0) {
            if ($this->current_page > $this->amount)
                $this->current_page = $this->amount;
        } else
            $this->current_page = 1;
    }

    private function amount()
    {
        $amout = intval($this->total / $this->limit);
        if ($this->total / $this->limit > $amout)
            $amout = $amout + 1;
        return $amout;
    }
}
